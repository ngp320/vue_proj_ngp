import request from '@/utils/request'

export function login(username, password) {
  return request({
    url: '/eduservice/user/login',
    method: 'post',
    data: {
      username,
      password
    }
  })
}
/**
 * F12控制台的network能看到每个请求都是两次
 * 第一次 request method options 测试请求是否能连通
 * 第二次 才去真正请求
 * */ 
export function getInfo(token) {
  return request({
    url: '/eduservice/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/eduservice/user/logout',
    method: 'post'
  })
}
