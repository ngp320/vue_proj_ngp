import request from '@/utils/request'
export default {
    /**
     * 1 查询讲师列表 
     * @param {Number} currentPage 当前页
     * @param {Number} pageSize 
     * @param {Object} teacherQuery 
     */
    getTeacherListPage(currentPage, pageSize, teacherQuery) {
      return request({
        // url: '/eduservice/teacher/pageTeacherCondition/'+ currentPage +'/'+pageSize ,
        // 推荐用下一种方式不容易漏掉/
        url: `/eduservice/teacher/pageTeacherCondition/${currentPage}/${pageSize}` ,
        method: 'post',
        // teacherQuery条件对象, 后端使用RequestBody获取数据
        // data表示吧对象转换成json传递到接口里
        data: teacherQuery
      })
    },
    /**
     * 根据id删除讲师
     */
    deleteTeacherById(id){
      return request({
        url: `/eduservice/teacher/${id}` ,
        method: 'delete'
      })
    },
    /**
     * 根据id查询讲师
     * @param {Object} teacher 
     */
    addTeacher(teacher){
      return request({
        url: `/eduservice/teacher/addTeacher` ,
        method: 'post',
        data: teacher
      })
    },
    /**
     * 根据id查询讲师
     */
    getTeacherInfo(id){
      return request({
        url: `/eduservice/teacher/getTeacher/${id}` ,
        method: 'get',
      })
    },
    /**
     * 根据id修改讲师
     * @param {Object} teacher 
     */
    updateTeacher(teacher){
      return request({
        url: `/eduservice/teacher/updateTeacher` ,
        method: 'post',
        data: teacher
      })
    }
}